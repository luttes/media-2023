.. index::
   pair: Fête à Macron; 2023-04-24

.. _macron_2023_04_24:

=======================================================
2023-04-24 **Mauvais anniversaire Macron !** |macron|
=======================================================

- :ref:`retraites:fete_a_macron_203_04_24`

.. figure:: casserolade_macron_2023_04_24.jpg
   :align: center
   :width: 500


.. figure:: casserolade_macron_2023_04_24_grenoble.jpg
   :align: center
   :width: 500


.. figure:: casserolade_macron_2023_04_24_grenoble_festif.jpg
   :align: center
   :width: 500

.. figure:: mauvais_anniversaire_a_macron_2023_04_24.jpg
   :align: center
   :width: 500
