.. index::
   ! Olivier Veran

.. _olivier_veran:

=========================================
Photos/dessins sur Olivier Veran
=========================================

:Source: https://nuage.gresille.org/index.php/s/zbdGxc4Tg38yYkH


La guerre c'est la liberté
============================


.. figure:: la_guerre_cest_la_liberte.png
   :align: center

   La guerre c'est la liberté


Je contribue à saboter le système public de la santé
============================================================

.. figure:: sabotage_du_systeme_de_sante.png
   :align: center

   Je contribue à saboter le système public de la santé



Il expérimente. Il est content de lui
==========================================

.. figure:: slogans_post_situs.png
   :align: center

   Il expérimente. Il est content de lui


Mais ça finit en 49.3
==========================

.. figure:: ca_finit_en_49_3.png
   :align: center

   Mais ça finit en 49.3

Pote avec l'extrêmiste de droite Darmanin
===========================================

.. figure:: pote_avec_darmanin.png
   :align: center

   Pote avec l'extrêmiste de droite Darmanin


Deux ans de plus
===========================================

.. figure:: deux_ans_de_plus.png
   :align: center

   Deux ans de plus


Un clown de plus
===========================================

.. figure:: nez_de_clown.png
   :align: center

   Un clown de plus


Ami, entends-tu le vol noir du Véran sur nos plaines ?
========================================================

- :ref:`retraites:veran_2023_05_12`

.. figure:: vol_noir.png
   :align: center

   Ami, entends-tu le vol noir du Véran sur nos plaines ?
