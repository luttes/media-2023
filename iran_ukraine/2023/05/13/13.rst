
.. index::
   pair: Iran-Ukraine; Samedi 13 mai 2023

.. _iran_ukraine_2023_05_13:

==================================================================================================
2023-05-13 **Manifestation avec nos camarades Ukrainiens sur la place Félix Poulat de Grenoble**
==================================================================================================

- :ref:`iran_luttes:iran_grenoble_2023_05_13`


.. raw:: html

   <iframe width="800" height="400" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"
       src="https://www.openstreetmap.org/export/embed.html?bbox=5.725057125091554%2C45.18901829435873%2C5.728592276573182%2C45.19056457227583&amp;layer=mapnik" style="border: 1px solid black">
   </iframe><br/>
   <small><a href="https://www.openstreetmap.org/#map=19/45.18979/5.72682">Afficher une carte plus grande pour situer la place Félix Poulat (Parvis de l'Abbé Pierre) </a></small>


.. figure:: iran_1.png
   :align: center


.. figure:: ukraine_1.png
   :align: center

.. figure:: iran_2.png
   :align: center


.. figure:: iran_ukraine_1.png
   :align: center


.. figure:: ukraine_2.png
   :align: center


.. figure:: iran_ukraine_2.png
   :align: center

.. figure:: missile_1.png
   :align: center


.. figure:: iran_3.png
   :align: center

