.. index::
   pair: Jeu ; Casserolade
   ! Casserolade

.. _casserolade_2023:

=========================================
Casserolade
=========================================

.. _caserolle_2023:

Ceci est une casserolle
===================================

.. figure:: casserolle.jpeg
   :align: center
   :width: 500



Jeu d'été
=============

.. figure:: jeu_ete.png
   :align: center
   :width: 500

jo_casseroles
=================

.. figure:: jo_casseroles.png
   :align: center
   :width: 500


Pas de retrait pas de paix
===========================

.. figure:: pas_de_retrait_pas_de_paix.png
   :align: center
   :width: 500

AGs, zbeul, entraide
===========================

.. figure:: ags_zbeul_entraide.png
   :align: center
   :width: 500

